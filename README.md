## Singularity Module example
This project is a very simple entry point for developers that wish to develop
and build their own modules for the Singularity platform.

### How it works
To build this project you need to clone this project repository and build and bundle 
it as a module using gradle. To do so use the following command:
```shell
gradlew shadowJar
```
After running the command you will find the bundled module under _build/libs/example-module-all.jar_.
To install and run operators of this module, you need to copy the .jar file into the _modules_ folder
under your Singularity Home. This is typically located under _%user_home%/.singularity/modules_
