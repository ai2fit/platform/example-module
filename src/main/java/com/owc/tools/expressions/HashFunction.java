package com.owc.tools.expressions;


import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.concurrent.Callable;

import com.owc.singularity.engine.tools.Ontology;
import com.owc.singularity.engine.tools.expression.ExpressionEvaluator;
import com.owc.singularity.engine.tools.expression.ExpressionParsingException;
import com.owc.singularity.engine.tools.expression.ExpressionType;
import com.owc.singularity.engine.tools.expression.FunctionDescription;
import com.owc.singularity.engine.tools.expression.internal.SimpleExpressionEvaluator;
import com.owc.singularity.engine.tools.expression.internal.function.AbstractFunction;
import com.owc.tools.Base64Tools;

public class HashFunction extends AbstractFunction {

    private static final Charset DEFAULT_CHARSET = Charset.forName("utf-8");

    public HashFunction() {
        super("text_transformation.hash", FunctionDescription.UNFIXED_NUMBER_OF_ARGUMENTS, Ontology.POLYNOMINAL);
    }

    @Override
    public ExpressionEvaluator compute(ExpressionEvaluator... inputEvaluators) throws ExpressionParsingException {
        return new SimpleExpressionEvaluator(new Callable<String>() {

            @Override
            public String call() throws Exception {
                MessageDigest digest = MessageDigest.getInstance("SHA-256");
                for (ExpressionEvaluator evaluator : inputEvaluators) {
                    switch (evaluator.getType()) {
                        case BOOLEAN:
                            digest.update(evaluator.getBooleanFunction().call().booleanValue() ? (byte) 0 : (byte) 1);
                            break;
                        case DATE:
                            digest.update(longToByteArray(evaluator.getDateFunction().call().toInstant().getEpochSecond()));
                            break;
                        case DOUBLE:
                            digest.update(longToByteArray(Double.doubleToLongBits(evaluator.getDoubleFunction().call())));
                            break;
                        case INTEGER:
                            digest.update(longToByteArray((long) evaluator.getDoubleFunction().call()));
                            break;
                        case STRING:
                            digest.update(evaluator.getStringFunction().call().getBytes(DEFAULT_CHARSET));
                            break;
                        default:
                            break;
                    }
                }
                return Base64Tools.encode(digest.digest());
            }

        }, ExpressionType.STRING, false);
    }

    @Override
    protected ExpressionType computeType(ExpressionType... inputTypes) {
        return ExpressionType.STRING;
    }

    public static byte[] longToByteArray(long value) {
        return new byte[] { (byte) (value >> 56), (byte) (value >> 48), (byte) (value >> 40), (byte) (value >> 32), (byte) (value >> 24), (byte) (value >> 16),
                (byte) (value >> 8), (byte) value };
    }

}
