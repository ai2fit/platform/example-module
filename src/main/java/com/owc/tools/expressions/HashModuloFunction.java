package com.owc.tools.expressions;


import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.MessageDigest;

import com.owc.singularity.engine.tools.Ontology;
import com.owc.singularity.engine.tools.expression.DoubleCallable;
import com.owc.singularity.engine.tools.expression.ExpressionEvaluator;
import com.owc.singularity.engine.tools.expression.ExpressionParsingException;
import com.owc.singularity.engine.tools.expression.ExpressionType;
import com.owc.singularity.engine.tools.expression.FunctionDescription;
import com.owc.singularity.engine.tools.expression.internal.SimpleExpressionEvaluator;
import com.owc.singularity.engine.tools.expression.internal.function.AbstractFunction;

public class HashModuloFunction extends AbstractFunction {

    private static final Charset DEFAULT_CHARSET = Charset.forName("utf-8");

    public HashModuloFunction() {
        super("text_transformation.hash_modulo", FunctionDescription.UNFIXED_NUMBER_OF_ARGUMENTS, Ontology.INTEGER);
    }

    @Override
    public ExpressionEvaluator compute(ExpressionEvaluator... inputEvaluators) throws ExpressionParsingException {
        return new SimpleExpressionEvaluator(new DoubleCallable() {

            @Override
            public double call() throws Exception {
                MessageDigest digest = MessageDigest.getInstance("SHA-256");
                if (inputEvaluators.length < 2) {
                    throw new ExpressionParsingException(
                            new IllegalArgumentException("hash_modulo requires at least two arguments, the first one the integer for calculating the modulo."));
                }

                ExpressionEvaluator moduloEvaluator = inputEvaluators[0];
                if (moduloEvaluator.getType() != ExpressionType.INTEGER) {
                    throw new ExpressionParsingException(new IllegalArgumentException("hash_modulo requires at least integer as first argument."));
                }
                long modulo = (long) moduloEvaluator.getDoubleFunction().call();

                for (int i = 1; i < inputEvaluators.length; i++) {
                    ExpressionEvaluator evaluator = inputEvaluators[i];
                    switch (evaluator.getType()) {
                        case BOOLEAN:
                            digest.update(evaluator.getBooleanFunction().call().booleanValue() ? (byte) 0 : (byte) 1);
                            break;
                        case DATE:
                            digest.update(longToByteArray(evaluator.getDateFunction().call().toInstant().getEpochSecond()));
                            break;
                        case DOUBLE:
                            digest.update(longToByteArray(Double.doubleToLongBits(evaluator.getDoubleFunction().call())));
                            break;
                        case INTEGER:
                            digest.update(longToByteArray((long) evaluator.getDoubleFunction().call()));
                            break;
                        case STRING:
                            digest.update(evaluator.getStringFunction().call().getBytes(DEFAULT_CHARSET));
                            break;
                        default:
                            break;
                    }
                }
                return new BigInteger(digest.digest()).mod(BigInteger.valueOf(modulo)).doubleValue();
            }

        }, ExpressionType.DOUBLE, false);
    }

    @Override
    protected ExpressionType computeType(ExpressionType... inputTypes) {
        return ExpressionType.STRING;
    }

    public static byte[] longToByteArray(long value) {
        return new byte[] { (byte) (value >> 56), (byte) (value >> 48), (byte) (value >> 40), (byte) (value >> 32), (byte) (value >> 24), (byte) (value >> 16),
                (byte) (value >> 8), (byte) value };
    }

}
