package com.owc.operator;


import java.util.*;

import com.owc.singularity.engine.metadata.AttributeMetaData;
import com.owc.singularity.engine.metadata.ExampleSetMetaData;
import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.metadata.preconditions.SimplePrecondition;
import com.owc.singularity.engine.metadata.rules.PassThroughRule;
import com.owc.singularity.engine.object.data.example.Attribute;
import com.owc.singularity.engine.object.data.example.AttributeRole;
import com.owc.singularity.engine.object.data.example.Example;
import com.owc.singularity.engine.object.data.example.ExampleSet;
import com.owc.singularity.engine.operator.Operator;
import com.owc.singularity.engine.operator.annotations.DefinesOperator;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.operator.error.UserError;
import com.owc.singularity.engine.ports.InputPort;
import com.owc.singularity.engine.ports.OutputPort;
import com.owc.singularity.engine.process.parameter.ParameterType;
import com.owc.singularity.engine.process.parameter.ParameterTypeAttribute;
import com.owc.singularity.engine.process.parameter.ParameterTypeBoolean;
import com.owc.singularity.engine.process.parameter.UndefinedParameterError;
import com.owc.singularity.engine.tools.Ontology;

/**
 * <p>
 * This operator select the attributes of an {@link ExampleSet} based on selection set. Therefore,
 * attributes that are absent in the set are not kept.
 * </p>
 *
 * @author Hatem Hamad
 */
@DefinesOperator(module = "dummy-module", key = "select_attributes_by_data", group = "selection", name = "Select Attributes by Data", shortName = "Select Attributes by Data", icon = "toolkit.png")
public class SelectAttributesByData extends Operator {

    public static final String PORT_INPUT_EXAMPLE_SET_NAME = "example set input";
    public static final String PORT_INPUT_ATTRIBUTE_SET = "specification set input";
    public static final String PORT_OUTPUT_EXAMPLE_SET_NAME = "example set output";
    public static final String PORT_OUTPUT_ORIGINAL_NAME = "original";
    public static final String ATTRIBUTE_NAME_ATTRIBUTE_SET_ATTRIBUTE = "Name";

    public static final String PARAMETER_SELECT_SPECIAL_FEATURES = "include_special_attributes";
    public static final String PARAMETER_INVERT_SELECTION = "invert_selection";
    public static final String PARAMETER_ATTRIBUTE_NAME = "name_attribute";

    private static final String OPERATOR_NAME = "Select Attributes by Data";

    private final InputPort exampleSetInput = getInputPorts().createPort(PORT_INPUT_EXAMPLE_SET_NAME);
    private final InputPort attributeSetInput = getInputPorts().createPort(PORT_INPUT_ATTRIBUTE_SET);
    private final OutputPort exampleSetOutput = getOutputPorts().createPort(PORT_OUTPUT_EXAMPLE_SET_NAME);
    private final OutputPort originalOutput = getOutputPorts().createPort(PORT_OUTPUT_ORIGINAL_NAME);

    public SelectAttributesByData() {

        exampleSetInput.addPrecondition(new SimplePrecondition(exampleSetInput, getRequiredMetaData()));
        attributeSetInput.addPrecondition(new SimplePrecondition(attributeSetInput,
                new ExampleSetMetaData(Collections.singletonList(new AttributeMetaData(ATTRIBUTE_NAME_ATTRIBUTE_SET_ATTRIBUTE, Ontology.NOMINAL)))));
        getTransformer().addRule(new PassThroughRule(exampleSetInput, exampleSetOutput, false) {

            @Override
            public MetaData modifyMetaData(MetaData metaData) {
                if (metaData instanceof ExampleSetMetaData) {
                    try {
                        SelectAttributesByData.this.modifyMetaData((ExampleSetMetaData) metaData);
                    } catch (UndefinedParameterError ignored) {
                    }
                }
                return metaData;
            }
        });
        getTransformer().addPassThroughRule(exampleSetInput, originalOutput);
    }

    /**
     * The given ExampleSet is already a clone of the input example set so that changing this
     * examples set does not affect the original one. Subclasses should avoid cloning again
     * unnecessarily.
     */
    public ExampleSet apply(ExampleSet exampleSet, Set<String> attributeNamesSet) throws OperatorException {
        Iterator<AttributeRole> attributeRoleIterator = exampleSet.getAttributes().allAttributeRoles();
        while (attributeRoleIterator.hasNext()) {
            AttributeRole role = attributeRoleIterator.next();
            if (!keepAttribute(attributeNamesSet, role)) {
                attributeRoleIterator.remove();
            }
            checkForStop();
        }
        return exampleSet;
    }

    /**
     * Whether to keep an attribute or not
     * 
     * @param attributeNamesSet
     *            the set on attribute names to keep
     * @param attributeRole
     *            the attribute to validate
     * @return true iff the attribute should be kept
     */
    protected boolean keepAttribute(Set<String> attributeNamesSet, AttributeRole attributeRole) {
        return attributeNamesSet.contains(attributeRole.getAttribute().getName());
    }

    /**
     * Subclasses might override this method to define the meta data transformation performed by
     * this operator.
     *
     * @throws UndefinedParameterError
     *             if undefined parameters were accessed
     */
    protected MetaData modifyMetaData(ExampleSetMetaData metaData) throws UndefinedParameterError {
        metaData.attributesAreSubset();
        return metaData;
    }

    /**
     * Returns the example set input port, e.g. for adding errors.
     */
    protected final InputPort getInputPort() {
        return exampleSetInput;
    }

    /**
     * Subclasses might override this method to define more precisely the meta data expected by this
     * operator.
     */
    protected ExampleSetMetaData getRequiredMetaData() {
        return new ExampleSetMetaData();
    }

    public InputPort getExampleSetInputPort() {
        return exampleSetInput;
    }

    public OutputPort getExampleSetOutputPort() {
        return exampleSetOutput;
    }


    @Override
    public List<ParameterType> getParameterTypes() {
        final List<ParameterType> types = super.getParameterTypes();
        types.add(new ParameterTypeAttribute(PARAMETER_ATTRIBUTE_NAME, "The name attribute", attributeSetInput, false, Ontology.NOMINAL));
        types.add(new ParameterTypeBoolean(PARAMETER_SELECT_SPECIAL_FEATURES, "", false, false));
        types.add(new ParameterTypeBoolean(PARAMETER_INVERT_SELECTION, "", false, false));
        return types;
    }

    @Override
    public void doWork() throws OperatorException {
        final ExampleSet inputExampleSet = exampleSetInput.getData(ExampleSet.class);
        final ExampleSet applySet = (ExampleSet) inputExampleSet.clone();
        Set<String> attributesToKeep = readAttributesFrom(attributeSetInput.getData(ExampleSet.class));

        final boolean invertSelection = getParameterAsBoolean(PARAMETER_INVERT_SELECTION);
        boolean includeSpecial = getParameterAsBoolean(PARAMETER_SELECT_SPECIAL_FEATURES);

        if (invertSelection) {
            // select all other attributes
            Set<String> negatedAttributeSet = new LinkedHashSet<>(inputExampleSet.size());
            final Iterator<AttributeRole> allAttributesIterator = !includeSpecial ? inputExampleSet.getAttributes().regularAttributes()
                    : inputExampleSet.getAttributes().allAttributeRoles();
            while (allAttributesIterator.hasNext()) {
                final AttributeRole attributeRole = allAttributesIterator.next();
                if (!attributesToKeep.contains(attributeRole.getAttribute().getName())) {
                    negatedAttributeSet.add(attributeRole.getAttribute().getName());
                }
            }
            attributesToKeep = negatedAttributeSet;
        }

        if (!includeSpecial) {
            // add special attributes by default
            final Iterator<AttributeRole> specialAttributeIterator = inputExampleSet.getAttributes().specialAttributes();
            while (specialAttributeIterator.hasNext()) {
                final String specialAttribute = specialAttributeIterator.next().getAttribute().getName();
                attributesToKeep.add(specialAttribute);
            }
        }

        ExampleSet result = apply(applySet, attributesToKeep);

        originalOutput.deliver(inputExampleSet);
        exampleSetOutput.deliver(result);
    }

    private Set<String> readAttributesFrom(ExampleSet attributeNamesExampleSet) throws UserError {

        final String attributeNameKey = getParameterAsString(PARAMETER_ATTRIBUTE_NAME);
        final Attribute nameAttribute = attributeNamesExampleSet.getAttributes().get(attributeNameKey);
        if (nameAttribute == null) {
            throw new UserError(this, "toolkit.attribute_not_found_parameter", attributeNameKey);
        }
        if (!nameAttribute.isNominal()) {
            throw new UserError(this, "toolkit.1", nameAttribute.getName(), OPERATOR_NAME, attributeNameKey);
        }
        Set<String> attributeSet = new LinkedHashSet<>(attributeNamesExampleSet.size());
        for (Example example : attributeNamesExampleSet) {
            attributeSet.add(example.getNominalValue(nameAttribute));
        }
        return attributeSet;
    }

    @Override
    public boolean shouldAutoConnect(OutputPort port) {
        if (port == originalOutput) {
            return false;
        } else {
            return super.shouldAutoConnect(port);
        }
    }
}
