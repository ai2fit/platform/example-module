package com.owc.operator;


import java.util.List;

import com.owc.singularity.engine.metadata.AttributeMetaData;
import com.owc.singularity.engine.metadata.ExampleSetMetaData;
import com.owc.singularity.engine.metadata.MetaData;
import com.owc.singularity.engine.object.data.example.Attribute;
import com.owc.singularity.engine.object.data.example.Example;
import com.owc.singularity.engine.object.data.example.ExampleSet;
import com.owc.singularity.engine.object.data.example.table.AttributeFactory;
import com.owc.singularity.engine.operator.AbstractExampleSetProcessing;
import com.owc.singularity.engine.operator.annotations.DefinesOperator;
import com.owc.singularity.engine.operator.error.OperatorException;
import com.owc.singularity.engine.process.parameter.ParameterType;
import com.owc.singularity.engine.process.parameter.ParameterTypeDouble;
import com.owc.singularity.engine.process.parameter.ParameterTypeString;
import com.owc.singularity.engine.process.parameter.UndefinedParameterError;
import com.owc.singularity.engine.tools.Ontology;

/**
 * This operator will add a sequence attribute to the example set.
 *
 * @author Sebastian Land
 */
@DefinesOperator(module = "dummy-module", key = "generate_sequence", group = "generation", name = "Generate Sequence", shortName = "Generate Sequence", icon = "toolkit.png")
public class GenerateSequenceOperator extends AbstractExampleSetProcessing {

    public static final String PARAMETER_SEQUENCE_ATTRIBUTE_NAME = "new_attribute";
    public static final String PARAMETER_SEQUENCE_INCREMENT = "increment";
    public static final String PARAMETER_SEQUENCE_OFFSET = "offset";


    public GenerateSequenceOperator() {
    }

    @Override
    public ExampleSet apply(ExampleSet exampleSet) throws OperatorException {
        String nameOfAttribute = getParameterAsString(PARAMETER_SEQUENCE_ATTRIBUTE_NAME);
        double increment = getParameterAsDouble(PARAMETER_SEQUENCE_INCREMENT);
        double offset = getParameterAsDouble(PARAMETER_SEQUENCE_OFFSET);

        Attribute sequenceAttribute = AttributeFactory.createAttribute(nameOfAttribute, Ontology.REAL);
        exampleSet.getExampleTable().addAttribute(sequenceAttribute);
        exampleSet.getAttributes().addRegular(sequenceAttribute);

        for (Example example : exampleSet) {
            example.setValue(sequenceAttribute, offset);
            offset += increment;
        }
        return exampleSet;
    }

    @Override
    protected MetaData modifyMetaData(ExampleSetMetaData metaData) throws UndefinedParameterError {
        metaData.addAttribute(new AttributeMetaData(getParameterAsString(PARAMETER_SEQUENCE_ATTRIBUTE_NAME), Ontology.REAL));
        return metaData;
    }

    @Override
    public List<ParameterType> getParameterTypes() {
        List<ParameterType> types = super.getParameterTypes();

        types.add(new ParameterTypeString(PARAMETER_SEQUENCE_ATTRIBUTE_NAME,
                "The name of the newly create sequence attribute. No attribute with the same name may exist already.", "SequenceIndex"));
        types.add(new ParameterTypeDouble(PARAMETER_SEQUENCE_INCREMENT, "The increment from example to example.", Double.NEGATIVE_INFINITY,
                Double.POSITIVE_INFINITY, 1d));
        types.add(new ParameterTypeDouble(PARAMETER_SEQUENCE_OFFSET, "The start value for the first example in the given example set.",
                Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 1d));

        return types;
    }
}
