/**
 *
 */
package com.owc.singularity.module;


import com.owc.singularity.engine.modules.annotations.InitGUI;
import com.owc.singularity.engine.modules.annotations.InitModule;
import com.owc.singularity.engine.tools.expression.Constant;
import com.owc.singularity.engine.tools.expression.ExpressionParserModule;
import com.owc.singularity.engine.tools.expression.ExpressionRegistry;
import com.owc.singularity.engine.tools.expression.Function;
import com.owc.singularity.engine.tools.expression.internal.SimpleConstant;
import com.owc.singularity.studio.gui.MainFrame;
import com.owc.tools.expressions.*;

import java.util.LinkedList;
import java.util.List;

/**
 * This class provides hooks for initialization
 *
 * @author Sebastian Land
 */
public class JackhammerModule {

    public static final String PRODUCT_NAME = "dummy-module";
    /**
     * This method will be called directly after the extension is initialized. This is the first
     * hook during start up. No initialization of the operators or renderers has taken place when
     * this is called.
     */
    @InitModule
    public static void initPlugin() {
        ExpressionRegistry.INSTANCE.register(new ExpressionParserModule() {

            @Override
            public String getKey() {
                return "core.function_constants";
            }

            @Override
            public List<Function> getFunctions() {
                LinkedList<Function> list = new LinkedList<>();
                list.add(new HashFunction());
                list.add(new HashModuloFunction());
                return list;
            }

            @Override
            public List<Constant> getConstants() {
                LinkedList<Constant> list = new LinkedList<>();
                list.add(new SimpleConstant("MILLISECONDS_PER_YEAR", 31556952000.0, "Milliseconds per average year. Includes leap days."));
                list.add(new SimpleConstant("MILLISECONDS_PER_DAY", 86400000.0, "Milliseconds per day."));
                list.add(new SimpleConstant("MILLISECONDS_PER_WEEK", 604800000.0, "Milliseconds per week."));
                return list;
            }
        });
    }

    /**
     * This method is called during start up as the second hook. It is called before the gui of the
     * mainframe is created. The Mainframe is given to adapt the gui. The operators and renderers
     * have been registered in the meanwhile.
     */
    @InitGUI
    public static void initGui(MainFrame mainframe) {

    }
}
